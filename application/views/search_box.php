<style>
	div.search-block{
		margin-bottom: 0px !important;
	}
	.modal {
	  text-align: center;
	  padding: 0!important;
	}

	.modal:before {
	  content: '';
	  display: inline-block;
	  height: 100%;
	  vertical-align: middle;
	  margin-right: -4px;
	}

	.modal-dialog {
	  display: inline-block;
	  text-align: left;
	  vertical-align: middle;
	}
	.nama-detail, .nim-detail{
		display: inline-block;
		vertical-align: baseline;
	}
	ul.list-inline > li{
		padding-top: 5px;
		padding-bottom: 5px;
	}
</style>
<div class="search-block parallaxBg" style="background-position: 70% 20px;">
	<div class="container">
		<div class="col-md-6 col-md-offset-3">
			<h1>Cari <span class="color-green">alumni</span> ums</h1>

			<div class="input-group">
				<input id="param_alumni" type="text" class="form-control" placeholder="Cari berdasarkan nama atau NIM" style="text-transform: uppercase;">
				<span class="input-group-btn">
					<button id="btn_cari" class="btn-u btn-u-lg" type="submit"><i class="fa fa-search"></i></button>
				</span>
			</div>

			<!-- <form action="" class="sky-form page-search-form">
				<div class="inline-group">
					<label class="checkbox"><input type="checkbox" name="checkbox-inline" checked=""><i></i>Recent</label>
					<label class="checkbox"><input type="checkbox" name="checkbox-inline"><i></i>Related</label>
					<label class="checkbox"><input type="checkbox" name="checkbox-inline"><i></i>Popular</label>
					<label class="checkbox"><input type="checkbox" name="checkbox-inline"><i></i>Most Common</label>
				</div>
			</form> -->
		</div>
	</div>
</div>
<div id="modal_detail" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body col-md-12">

					<div class="row clients-page">
						<div class="col-md-3 foto-detail">

						</div>
						<div class="col-md-9">
							<h2 class="nama-detail"></h2><h3 class="nim-detail pull-right" style=""></h3>
							<ul class="list-inline">
								<li class="alamat-detail"></li><br>
								<li class="ttl-detail"></li> | <li class="gender-detail"></li> <br>
								<li class="email-detail"></li> | <li class="telp-detail"></li> <br>
								<li class="ipk-detail"></li>
							</ul>
							<p class="judul-detail"></p>
						</div>
					</div>

			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-md-6">
						<?php
                        if ($this->session->userdata('auth')) {
                            ?>
						<button type="button" id="btn-skpi" class="btn-u btn-u-sea"><i class="fa fa-file-pdf-o"></i> Dokumen SKPI</button>
						<button type="button" id="btn-ijazah" class="btn-u btn-u-blue"><i class="fa fa-file-image-o"></i> Ijazah</button>
						<?php
                        }
                        ?>
					</div>
					<div class="col-md-3 col-md-offset-3">
						<button type="button" class="btn-u" data-dismiss="modal"><i class="fa fa-undo"></i> Kembali</button>
					</div>
				</div>

			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

		<!--=== Search Block ===-->
		<?php $this->view('search_box'); ?>
		<!--/container-->
		<!--=== End Search Block ===-->
<style>
	.content-sm{
		padding-top: 20px !important;
		padding-bottom: 20px !important;
	}
</style>
<div class="container content-sm" hidden="true">
	<span class=""></span>

	<!-- Begin Inner Results -->
	<div class="div-result table-search-v1 panel panel-dark">
		<div class="panel-heading">
			<h3 class="results-number panel-title"></h3>
		</div>
		<div class="table-responsive">
			<table id="tabel_hasil" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th><small>Nama</small></th>
						<th><small>Kelamin</small></th>
						<th><small>NIM</small></th>
						<th><small>Tempat, tanggal lahir</small></th>
						<th><small>Detail</small></th>
					</tr>
				</thead>
				<tbody class="tbody-cari"></tbody>
			</table>
		</div>
	</div>
	<!-- <div class="inner-results">
		<h3><a href="#">Unify - Responsive Website Template</a></h3>
		<ul class="list-inline up-ul">
			<li>https://wrapbootstrap.com/theme/unify-responsive-website-template-WB0412697</li>
			<li class="btn-group">
				<button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button">
					More<i class="fa fa-caret-down margin-left-5"></i>
					<span class="sr-only">Toggle Dropdown</span>
				</button>
				<ul role="menu" class="dropdown-menu">
					<li><a href="#">Share</a></li>
					<li><a href="#">Similar</a></li>
					<li><a href="#">Advanced search</a></li>
				</ul>
			</li>
			<li><a href="#">Wrapbootstrap</a></li>
			<li><a href="#">Dribbble</a></li>
		</ul>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut orci urna. Morbi blandit enim eget risus posuere dapibus. Vestibulum velit nisi, tempus in placerat non, auctor eu purus. Morbi suscipit porta libero, ac tempus tellus consectetur non. Praesent eget consectetur nunc. Aliquam erat volutpat. Suspendisse ultrices eros eros, consectetur facilisis urna posuere id.</p>
		<ul class="list-inline down-ul">
			<li>
				<ul class="list-inline star-vote">
					<li><i class="color-green fa fa-star"></i></li>
					<li><i class="color-green fa fa-star"></i></li>
					<li><i class="color-green fa fa-star"></i></li>
					<li><i class="color-green fa fa-star"></i></li>
					<li><i class="color-green fa fa-star-half-o"></i></li>
				</ul>
			</li>
			<li>3 years ago - By Anthon Brandley</li>
			<li>234,034 views</li>
			<li><a href="#">Web designer</a></li>
		</ul>
	</div> -->

	<!-- Begin Inner Results -->

	<!-- <div class="margin-bottom-30"></div> -->

	<!-- <div class="text-left">
		<ul class="pagination">
			<li><a href="#">«</a></li>
			<li class="active"><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">...</a></li>
			<li><a href="#">157</a></li>
			<li><a href="#">158</a></li>
			<li><a href="#">»</a></li>
		</ul>
	</div> -->
</div>
<?php
$base_url = base_url();
$script = <<<EOT
let alumni_nama, alumni_skpi_url, alumni_photo_ijazah = "";
	$(document).ready(function(){
		$('#param_alumni').on('focus', function(){
			$(this).select();
		});
		$('#btn_cari').on('click', function(){
			$.ajax({
				url: '{$base_url}alumni/get_alumni',
				type: 'POST',
				dataType: 'JSON',
				data: {
					'param': $('#param_alumni').val(),
				},
				success: function(response){
					if(response.length !== 0){
						$('span.results-number').html('');
						$('tbody.tbody-cari').html('');
						$('h3.results-number').html('<i class="fa fa-globe"></i>DITEMUKAN '+response.length+' HASIL PENCARIAN');
						$('div.content-sm').prop('hidden', false);
						$('tbody.tbody-cari');
						var result = '';
						$.each(response, function(idx, val){
							result += '<tr>';
							result += '<td><small>'+val['nama']+'</small></td>';
							result += '<td><small>'+val['kelamin']+'</small></td>';
							result += '<td><small>'+val['nim']+'</small></td>';
							result += '<td><small>'+val['ttl']+'</small></td>';
							result += '<td><button class="btn btn-xs btn-success btn-detail-mhs" data-nim="'+val['nim']+'">Detail</button></td>';
							result += '</tr>';
						});
						$('tbody.tbody-cari').append(result);
					} else {
						$('div.content-sm').prop('hidden', true);
					}
				}
			});
		});

		$('#tabel_hasil').on('click', 'td > button.btn-detail-mhs',function(){
			// $('#modal_detail').modal('show');
			var nim = $(this).data('nim');
			// alert(nim);
			$.ajax({
				url: '{$base_url}alumni/get_detail_alumni',
				type: 'POST',
				dataType: 'JSON',
				data: {
					'nim': nim
				},
				success: function(response){
					console.log(response);
					alumni_nama = response.nama;
					if (response.hasOwnProperty('fileSkpi')) {
						alumni_skpi_url = response.fileSkpi;
						alumni_photo_ijazah = response.fileIjazah;
					}
					$('h2.nama-detail').html('');
					$('h2.nama-detail').append(response.nama);
					$('h3.nim-detail').html('');
					$('h3.nim-detail').append('<span class="color-blue">'+response.nim+'</span>');
					$('.alamat-detail').html('');
					$('.alamat-detail').append('<i class="fa fa-map-marker color-green"></i> '+response.alamat);
					$('.ttl-detail').html('');
					$('.ttl-detail').append('<i class="fa fa-user color-green"></i> '+response.ttl);
					$('.gender-detail').html('');
					$('.gender-detail').append('<i class="fa fa-intersex color-green"></i> '+response.kelamin);
					$('.email-detail').html('');
					$('.email-detail').append('<i class="fa fa-at color-green"></i> '+response.email);
					$('.telp-detail').html('');
					$('.telp-detail').append('<i class="fa fa-phone color-green"></i> '+response.telp);
					$('.ipk-detail').html('');
					$('.ipk-detail').append('<h5><i class="fa fa-university color-green"></i>IPK: '+response.ipk+', '+response.predikat+'</h5>');
					$('.judul-detail').html('');
					$('.judul-detail').append(response.judul_skripsi);
					$('.foto-detail').html('<img src="https://lulusan.ums.ac.id/assets/foto_mhs/'+response.foto+'" class="img-responsive hover-effect" alt="">');

					$('#modal_detail').modal('show');
					if (typeof skpiWindow !== 'undefined') {
						skpiWindow.close();
					} else if (typeof ijazahWindow !== 'undefined') {
						ijazahWindow.close();
					}
				}

			});
		});
		var winid = 1;
		$('#btn-skpi').on('click', function(){
			$('#modal_detail').modal('hide');
			skpiWindow = dhtmlwindow.open("broadcastbox", "iframe", "http://localhost/project/BAA/alumni/assets/foto_mhs/mpdf.pdf", "Dokumen SKPI :: "+alumni_nama,
			"width=800px,height=450px,left=260px,top=665px,resize=1,scrolling=0,center=1",
			"recal"+winid);
			winid++;
		});
		$('#btn-ijazah').on('click', function(){
			$('#modal_detail').modal('hide');
			ijazahWindow = dhtmlwindow.open("broadcastbox", "iframe", "http://localhost/project/BAA/alumni/assets/img/user.jpg", "Dokumen Ijazah :: "+alumni_nama,
			"width=800px,height=450px,left=260px,top=665px,resize=1,scrolling=0,center=1",
			"recal"+winid);
			winid++;
		});
	});
EOT;
$this->session->set_flashdata('footer_script', $script);

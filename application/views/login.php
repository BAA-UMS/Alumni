<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!--=== Breadcrumbs ===-->
		<div class="breadcrumbs">
			<div class="container">
				<h1 class="pull-left">Login</h1>
				<ul class="pull-right breadcrumb">
					<li><a href="<?php echo base_url(); ?>">Home</a></li>
					<li><a href="#">Pages</a></li>
					<li class="active">Login</li>
				</ul>
			</div><!--/container-->
		</div><!--/breadcrumbs-->
		<!--=== End Breadcrumbs ===-->

		<!--=== Content Part ===-->
		<div class="container content">
			<div class="row">
				<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
					<?php echo form_open('/user/user_login'); ?>
						<div class="reg-header">
							<h2>Masuk ke account anda</h2>
						</div>

						<div class="input-group margin-bottom-20">
							<span class="input-group-addon"><i class="fa fa-user"></i></span>
							<input type="text" name="username" placeholder="Username" class="form-control">
						</div>
						<div class="input-group margin-bottom-20">
							<span class="input-group-addon"><i class="fa fa-lock"></i></span>
							<input type="password" name="password" placeholder="Password" class="form-control">
						</div>

						<div class="row">
							<div class="col-md-6 checkbox">
								<label><input type="checkbox"> Tetap login</label>
							</div>
							<div class="col-md-6">
								<button class="btn-u pull-right" type="submit">Login</button>
							</div>
						</div>
						<hr>
					<?php echo form_close(); ?>
				</div>
			</div><!--/row-->
		</div><!--/container-->
        <!--=== End Content Part ===-->

<?php
$script = <<<EOF
$(document).ready(function() {
    $("form").submit(function(e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            dataType: 'JSON',
            data: $(this).serialize(),
            success: function(response){
                alert('login sukses');
                window.location = response.href;
            },
            error: function() {
                alert('username atau password salah');
            }
        });
    });
    });
EOF;
$this->session->set_flashdata('footer_script', $script);

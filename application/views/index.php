<!DOCTYPE html>
<html>
<head>
<title>Lulusan | Universitas Muhammadiyah Surakarta</title>

<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<!-- Favicon -->
<link rel="shortcut icon" href="favicon.ico">

<!-- Web Fonts -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/google_fonts.css">

<!-- CSS Global Compulsory -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">

<!-- CSS Header and Footer -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/headers/header-default.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/footers/footer-v1.css">

<!-- CSS Implementing Plugins -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/animate.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/line-icons/line-icons.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/sky-forms-pro/skyforms/css/sky-forms.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css">
<!--[if lt IE 9]><link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/sky-forms-pro/skyforms/css/sky-forms-ie8.css"><![endif]-->

<!-- CSS Page Style -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/pages/page_search.css">

<!-- CSS Theme -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme-colors/default.css" id="style_color">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme-skins/dark.css">

<!-- CSS Customization -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">

<!-- DHTML Window -->
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/dhtmlwindow/dhtmlwindow.css'); ?>">
</head>

<body>
	<div class="wrapper">
		<!--=== Header ===-->
		<?php echo $header; ?>
		<!--=== End Header ===-->

		<!--=== Content ===-->
		<?php echo $content; ?>
		<!--/container-->
		<!--=== End Content ===-->
		<!--=== Footer Version 1 ===-->
		<?php echo $footer; ?>
		<!--=== End Footer Version 1 ===-->
	</div><!--/End Wrapepr-->
	<!--[if lt IE 9]>
	<script src="assets/plugins/respond.js"></script>
	<script src="assets/plugins/html5shiv.js"></script>
	<script src="assets/plugins/placeholder-IE-fixes.js"></script>
	<![endif]-->


	<div id="topcontrol" title="Scroll Back to Top" style="position: fixed; bottom: 5px; right: 5px; opacity: 0; cursor: pointer;"></div>
</body>
</html>
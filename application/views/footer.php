		<div class="footer-v1">
			<div class="footer">
				<div class="container">
					<div class="row">
						<!-- About -->

						<!-- End About -->

						<!-- Latest -->

						<!-- End Latest -->

						<!-- Link List -->

						<!-- End Link List -->

						<!-- Address -->
						<div class="col-md-3 map-img md-margin-bottom-40">
							<div class="headline"><h2>Contact Us</h2></div>
							<address class="md-margin-bottom-40">
								Jl. A. Yani, Pabelan, Kartasura,<br>
								Surakarta 57162, <br>
								Jawa Tengah, Indonesia<br>
								Email: <a href="mailto:info@anybiz.com" class="">ums@ums.ac.id</a><br>
								Phone: <a>+62 271 717417</a><br>
								Fax: <a>+62 271 725448</a><br>
							</address>
						</div><!--/col-md-3-->
						<!-- End Address -->
					</div>
				</div>
			</div><!--/footer-->

			<div class="copyright">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<p>
								2015 © All Rights Reserved.
								<a href="#">Privacy Policy</a> | <a href="#">Terms of Service</a>
							</p>
						</div>

						<!-- Social Links -->

						<!-- End Social Links -->
					</div>
				</div>
			</div><!--/copyright-->
		</div>
	<!-- JS Global Compulsory -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-migrate.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!-- JS Implementing Plugins -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/back-to-top.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/smoothScroll.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery.parallax.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/counter/waypoints.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/counter/jquery.counterup.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
	<!-- JS Customization -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom.js"></script>
	<!-- JS Page Level -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/owl-carousel.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/style-switcher.js"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/dhtmlwindow/dhtmlwindow.js'); ?>"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			App.init();
			App.initCounter();
			App.initParallaxBg();
			OwlCarousel.initOwlCarousel();
			StyleSwitcher.initStyleSwitcher();
		});
	</script>
<?php
if ($this->session->flashdata('footer_script')) {
    ?>
<script type="text/javascript">
    <?php echo $this->session->flashdata('footer_script'); ?>

</script>
<?php
}

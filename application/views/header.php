<div class="header">
	<div class="container">
		<!-- Logo -->
		<a class="logo" href="#">
			<img src="<?php echo base_url('assets/img/Logo_Baru.png');?>" alt="Logo" style="width: 10%">
		</a>
		<!-- End Logo -->

		<!-- Topbar -->
		<div class="topbar">
			<ul class="loginbar pull-right">
				<?php
                if ($this->session->userdata('auth')) {
                    ?><li><a href="<?php echo base_url('user/logout'); ?>">Logout</a></li>
				<?php
                } else {
                    ?><li><a href="<?php echo base_url('user/login'); ?>">Login</a></li>
				<?php
                }
                ?>
			</ul>
		</div>
		<!-- End Topbar -->

		<!-- Toggle get grouped for better mobile display -->
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="fa fa-bars"></span>
		</button>
		<!-- End Toggle -->
	</div><!--/end container-->

	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse mega-menu navbar-responsive-collapse">
		<div class="container">
			<ul class="nav navbar-nav">
				<!-- Home -->
				<li class=" active">
					<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
						Home
					</a>
				</li>
				<!-- End Home -->

				<!-- Search Block -->
				<!-- <li>
					<i class="search fa fa-search search-btn"></i>
					<div class="search-open">
						<div class="input-group animated fadeInDown">
							<input type="text" class="form-control" placeholder="Search">
							<span class="input-group-btn">
								<button class="btn-u" type="button">Go</button>
							</span>
						</div>
					</div>
				</li> -->
				<!-- End Search Block -->
			</ul>
		</div><!--/end container-->
	</div><!--/navbar-collapse-->
</div>
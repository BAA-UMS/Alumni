<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alumni_model extends CI_Model {

	function get_alumni_model($inputs){
		$param = !empty($inputs['param']) ? $inputs['param']: '';
		$q = '';
		if($param){
			// $this->db->select('FNAMA,JN_LAMIN,FNIM,FTMPLAHIR,DATE_FORMAT(FTGLLAHIR,  "%Y-%m-%d") FTGLLAHIR');
			$this->db->select('FNAMA,JN_LAMIN,FNIM,FTMPLAHIR,CONVERT(VARCHAR(24),FTGLLAHIR,21) AS FTGLLAHIR');
			$this->db->where('FNIM', $param);
			$this->db->or_like('FNAMA', $param);
			$q = $this->db->get('umhsdetail');
		}

		return $q;
	}

	function get_detail_alumni_model($inputs){
		$nim = !empty($inputs['nim']) ? $inputs['nim']: '';
		$q = '';
		if($nim){
			$q = $this->db->query("
				SELECT A.FNAMA
					,A.JN_LAMIN
					,A.FNIM
					,A.FTMPLAHIR
					-- ,DATE_FORMAT(A.FTGLLAHIR, '%Y-%m-%d') FTGLLAHIR
					,CONVERT(VARCHAR(24),A.FTGLLAHIR,21) FTGLLAHIR
					,B.FIPK
					,B.FPredikatLulus
					,A.FJSkripsi
					,B.FPotho
					,B.Alamat
					,B.email
					,B.Tlp
				FROM umhsdetail A
				LEFT JOIN zwisudawan B ON B.FNIM = A.FNIM
				WHERE A.FNIM = '".$nim."'
			");
		}
		return $q;
	}
}
/* End of file Alumni_model.php */
/* Location: ./application/models/Alumni_model.php */
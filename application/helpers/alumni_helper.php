<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('render')) {
    function render($page)
    {
        $CI = &get_instance();
        $data['header'] = $CI->load->view('header', null, true);
        $data['content'] = $CI->load->view($page, null, true);
        $data['footer'] = $CI->load->view('footer', null, true);

        return $CI->load->view('index', $data);
    }
}

if (!function_exists('response')) {
    /**
     * Response
     *
     * Meng-handle Output / Responses yang berformat JSON
     *
     * @param output
     * Array responses
     *
     * @param statusCode
     * Http Status Code
     *
     * @return json
     */
    function response(array $output, $statusCode)
    {
        $CI = &get_instance();
        return $CI->output
            ->set_status_header($statusCode)
            ->set_content_type('application/json')
            ->set_output(json_encode($output, JSON_PRETTY_PRINT));
    }
}

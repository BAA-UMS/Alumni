<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    public function login()
    {
        $this->load->helper('form');
        return render('login');
    }

    public function user_login()
    {
        if (!$this->input->is_ajax_request() && !$this->input->post()) {
            return redirect(base_url('login'));
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'username', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        if (!$this->form_validation->run()) {
            return response([
                'errors' => $this->form_validation->error_array()
            ], 400);
        }
        $username = "kemal";
        $password = "naruto123";
        if ($this->input->post('username') == $username && $this->input->post('password') == $password) {
            $session = [
                'username' => $username
            ];
            $this->session->set_userdata('auth', $session);
            return response(['success' => true, 'href' => base_url()], 200);
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        return redirect(base_url('user/login'));
    }
}

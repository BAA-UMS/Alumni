<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Alumni extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('alumni_model');
    }

    public function index()
    {
        $data = array();
        return render('search_result');
    }

    public function date_to_indo($date)
    { // fungsi atau method untuk mengubah tanggal ke format indonesia
        // variabel BulanIndo merupakan variabel array yang menyimpan nama-nama bulan
        $bulan_indo = array("JANUARI", "FEBRUARI", "MARET",
        "APRIL", "MEI", "JUNI",
        "JULI", "AGUSTUS", "SEPTEMBER",
        "OKTOBER", "NOVEMBER", "DESEMBER");

        $tahun = substr($date, 0, 4); // memisahkan format tahun menggunakan substring
        $bulan = substr($date, 5, 2); // memisahkan format bulan menggunakan substring
        $tgl   = substr($date, 8, 2); // memisahkan format tanggal menggunakan substring

        $result = $tgl . " " . $bulan_indo[(int)$bulan-1] . " ". $tahun;
        return($result);
    }

    public function get_alumni()
    {
        $inputs = $this->input->post();
        // $data = $this->alumni_model->get_alumni_model($inputs);
        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($inputs)
            ),
            "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            )
        );
        $context = stream_context_create($options);
        $result = file_get_contents("https://lulusan.ums.ac.id/alumni/get_alumni", false, $context);

        return print_r($result);
    }

    public function get_detail_alumni()
    {
        $inputs = $this->input->post();
        // $data_detail = $this->alumni_model->get_detail_alumni_model($inputs);
        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($inputs)
            ),
            "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            )
        );
        $context = stream_context_create($options);
        $result = file_get_contents("https://lulusan.ums.ac.id/alumni/get_detail_alumni", false, $context);
        if ($this->session->userdata('auth')) {
            $fileName = ''; // database
            $tahunLulus = ''; // database
            $idFakultas = ''; // database
            $fileSkpi = base_url('assets/'.$idFakultas.'/'.$tahunLulus.'/'.$fileName);
            $fileIjazah = base_url('assets/' . $idFakultas . '/' . $tahunLulus . '/' . $fileName);

            $result = array_merge(json_decode($result, true), [
                'fileSkpi'  => $fileSkpi,
                'fileIjazah'=> $fileIjazah
            ]);
        }
        return print_r(json_encode($result));
    }
}
